import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
    changeCocktailFailure, changeCocktailRequest,
    changeCocktailSuccess,
    createCocktailFailure, createCocktailRequest,
    createCocktailSuccess,
    fetchCocktailFailure, fetchCocktailRequest,
    fetchCocktailsFailure,
    fetchCocktailsRequest,
    fetchCocktailsSuccess, fetchCocktailSuccess, removeCocktailFailure, removeCocktailRequest, removeCocktailSuccess
} from "../actions/cocktailsActions";
import {historyPush} from "../actions/historyActions";


export function* fetchCocktails() {
    try {
        const response = yield axiosApi.get('/cocktails');
        yield put(fetchCocktailsSuccess(response.data));
    } catch (error) {
        yield put(fetchCocktailsFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* fetchCocktail({payload: id}) {
    try {
        const response = yield axiosApi.get('/cocktails/' + id);
        yield put(fetchCocktailSuccess(response.data));
    } catch (error) {
        console.log(error)
        yield put(fetchCocktailFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* createCocktail({payload: product}) {
    try {
        const response = yield axiosApi.post('/cocktails', product);
        yield put(createCocktailSuccess(response.data));
        toast.success('Created successful, but not still published');
        yield put(historyPush('/'));
    } catch (error) {
        yield put(createCocktailFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* changeCocktail({payload: id}) {
    try {
        const response = yield axiosApi.put('/cocktails/' + id + '/published');
        yield put(changeCocktailSuccess(response.data));
        yield put(historyPush('/'));
        toast.success('Posted successful')
    } catch (error) {
        yield put(changeCocktailFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* removeCocktail({payload: id}){
    try{
        yield axiosApi.delete('/cocktails/' + id);
        yield put(removeCocktailSuccess(id));
        toast.success('Deleted successfull')
    }catch(e){
        yield put(removeCocktailFailure(e.response));
        toast.error(e.response.data.global)
    }
}



const cocktailsSaga = [
    takeEvery(fetchCocktailsRequest, fetchCocktails),
    takeEvery(fetchCocktailRequest, fetchCocktail),
    takeEvery(createCocktailRequest, createCocktail),
    takeEvery(changeCocktailRequest, changeCocktail),
    takeEvery(removeCocktailRequest, removeCocktail),
]

export default cocktailsSaga;
