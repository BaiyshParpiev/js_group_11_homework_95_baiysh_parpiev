import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
    loginUserSuccess, loginUserFailure, facebookRequest, googleRequest, logoutUser
} from "../actions/usersToolkitActions";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";


export function* facebookLogin({payload: facebookData}) {
    try {
        const response = yield axiosApi.post('/users/facebookLogin', facebookData);
        yield put(loginUserSuccess(response.data.user));
        yield put(historyPush('/'));
        toast.success('Login successful')
    } catch (error) {
        console.log(error)
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* googleLogin({payload: googleData}) {
    try {
        const response = yield axiosApi.post('/users/googleLogin', {
            tokenId: googleData.tokenId,
            googleId: googleData.googleId,
        });
        yield put(loginUserSuccess(response.data.user));
        yield put(historyPush('/'));
        toast.success('Login successful')
    } catch (error) {
        yield put(loginUserFailure(error.response.data));
        toast.error(error.response.data.global);
    }
}

export function* logoutUserSaga()  {
        yield axiosApi.delete('/users/sessions');
        yield put(historyPush('/'));
}


const usersSaga = [
    takeEvery(facebookRequest, facebookLogin),
    takeEvery(googleRequest, googleLogin),
    takeEvery(logoutUser, logoutUserSaga),
]

export default usersSaga;
