import {createSlice} from '@reduxjs/toolkit';

const name = 'users';


export const initialState = {
    loginError: null,
    loginLoading: false,
    user: null,
};


const usersSlice = createSlice({
    name,
    initialState,
    reducers: {
        loginUserSuccess(state, {payload: userData}){
            state.loginLoading = false;
            state.user = userData;
        },
        loginUserFailure(state, {payload: error}){
            state.loginLoading = false;
            state.error = error;
        },
        googleRequest(state, action){
            state.loginLoading = true;
        },
        facebookRequest(state, action){
            state.loginLoading = true;
        },
        logoutUser(state, action){
            state.user = null;
        },
    },
});

export default usersSlice;