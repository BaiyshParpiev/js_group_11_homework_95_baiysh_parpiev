import {createSlice} from '@reduxjs/toolkit';

const name = 'cocktails';


export const initialState = {
    cocktails: [],
    cocktail: null,
    fetchLoading: false,
    fetchError: null,
    singleLoading: false,
    singleError: null,
    createProductLoading: false,
    createProductError: null,
};


const cocktailsSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchCocktailsRequest(state, action) {
            state.fetchLoading = true;
        },
        fetchCocktailsSuccess(state, {payload: products}) {
            state.fetchLoading = false;
            state.cocktails = products;
            state.fetchError = null;
        },
        fetchCocktailsFailure(state, {payload: error}){
            state.fetchLoading = false;
            state.fetchError = error;
        },
        fetchCocktailRequest(state, action){
            state.singleLoading = true;
        },
        fetchCocktailSuccess(state, {payload: product}){
            state.singleLoading = false;
            state.cocktail = product;
        },
        fetchCocktailFailure(state, {payload: error}){
            state.singleLoading = false;
            state.singleError = error;
        },
        createCocktailRequest(state, action){
            state.singleLoading = true;
        },
        createCocktailSuccess(state, {payload: newProduct}){
            state.singleLoading = false;
            state.cocktails = [...state.cocktails, newProduct];
        },
        createCocktailFailure(state, {payload: error}){
            state.singleLoading = false;
            state.singleError = error;
        },
        changeCocktailRequest(state, action){
            state.singleLoading = true;
        },
        changeCocktailSuccess(state, {payload: changedProduct}){
            state.singleLoading = false;
            state.cocktails.map(s => s._id === changedProduct._id ? changedProduct : s);
        },
        changeCocktailFailure(state, {payload: error}){
            state.singleLoading = false;
            state.singleError = error;
        },
        removeCocktailRequest(state, action){
            state.singleLoading = true;
        },
        removeCocktailSuccess(state, {payload: id}){
            state.cocktails = state.cocktails.filter(c => c._id !== id);
            state.singleLoading = false;
        },
        removeCocktailFailure(state, {payload: error}){
            state.singleError = error;
            state.singleLoading = false;
        }
    },
});

export default cocktailsSlice;