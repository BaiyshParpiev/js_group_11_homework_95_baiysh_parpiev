import usersSlice from "../slices/usersSlice";

export const {
    loginUserSuccess,
    loginUserFailure,
    facebookRequest,
    googleRequest,
    logoutUser,
} = usersSlice.actions;