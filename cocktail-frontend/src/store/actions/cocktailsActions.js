import cocktailsSlice from "../slices/cocktailsSlice";

export const {
    fetchCocktailsRequest,
    fetchCocktailsSuccess,
    fetchCocktailsFailure,
    fetchCocktailRequest,
    fetchCocktailSuccess,
    fetchCocktailFailure,
    createCocktailRequest,
    createCocktailSuccess,
    createCocktailFailure,
    changeCocktailRequest,
    changeCocktailSuccess,
    changeCocktailFailure,
    removeCocktailRequest,
    removeCocktailSuccess,
    removeCocktailFailure
} = cocktailsSlice.actions;