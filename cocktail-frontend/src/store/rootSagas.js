import {all} from 'redux-saga/effects';
import usersSaga from "./sagas/usersSagas";
import historySagas from "./sagas/historySagas";
import cocktailsSaga from "./sagas/cocktailsSagas";
import history from "../history";

export function* rootSagas() {
    yield all([
        ...usersSaga,
        ...historySagas(history),
        ...cocktailsSaga,
    ])
}

