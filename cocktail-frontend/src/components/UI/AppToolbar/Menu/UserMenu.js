import React, {useState} from 'react';
import {Avatar, Button, makeStyles, Menu, MenuItem, Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersToolkitActions";
import {deepPurple} from "@material-ui/core/colors";

const useStyles = makeStyles(theme => ({
    profile: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    [theme.breakpoints.down('sm')]: {
        profile: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
    },
    userName: {
        display: 'flex',
        alignItems: 'center'
    },
    brandContainer: {
        display: "flex",
        anItems: 'center'
    },
    purple: {
        color: theme.palette.getContrastText(deepPurple[500]),
        backgroundColor: deepPurple[500],
    }
}))

const UserMenu = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
                <div className={classes.profile}>
                    <Avatar className={classes.purple}
                            alt={user?.displayName}
                            src={user?.avatar}
                    >
                        {user?.displayName.charAt(0)}
                    </Avatar>
                    <Typography
                        className={classes.userName}
                        variant="h6"
                    >
                        {user.displayName}
                    </Typography>
                </div>
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem>Profile</MenuItem>
                <MenuItem>My account</MenuItem>
                <MenuItem onClick={() => dispatch(logoutUser())}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;