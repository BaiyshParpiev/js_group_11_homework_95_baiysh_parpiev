import {Card, CardActions, CardHeader, CardMedia, Grid, IconButton, makeStyles} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {Link} from "react-router-dom";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const CocktailItem = ({title, id, image, published}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.users);

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    return user.role === 'admin' ? (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardActions>
                    <IconButton component={Link} to={'/cocktail/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    ): (
        published && <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardActions>
                    <IconButton component={Link} to={'/cocktail/' + id}>
                        <ArrowForwardIcon />
                    </IconButton>
                </CardActions>
            </Card>
        </Grid>
    )
};


export default CocktailItem;