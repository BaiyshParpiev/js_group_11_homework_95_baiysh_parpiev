import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Button, Grid, TextField} from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const CocktailForm = ({onSubmit, error, loading}) => {
    const classes = useStyles();

    const [state, setState] = useState({
        title: "",
        recipe: "",
        image: null,
    });

    const [ingredients, setIngredients] = useState([{title: '', amount: ''}])

    const submitFormHandler = e => {
        e.preventDefault();
        const info = {...state};
        info.ingredients = JSON.stringify(ingredients);

        const formData = new FormData();
        Object.keys(info).forEach(key => {
            formData.append(key, info[key]);
        });

        onSubmit(formData);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const addIngredients = () => {
        setIngredients(prev => [
                ...prev,
               {title: '', amount: ''}
        ])
    }

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    };
    const ingredientsChangeHandler = (i, name, value) => {
        setIngredients(prev => {
            const ingCopy = {
                ...prev[i],
                [name]: value,
            };

            return prev.map((ing, index) => {
                if(i === index){
                    return ingCopy;
                }
                return ing;
            })
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
            noValidate
        >
            <FormElement
                required
                label="Title"
                name="title"
                value={state.title}
                onChange={inputChangeHandler}
                error={getFieldError('title')}
            />
            <FormElement
                required
                label="Recipe"
                name="recipe"
                multiline
                rows={4}
                value={state.recipe}
                onChange={inputChangeHandler}
                error={getFieldError('recipe')}
            />
            {ingredients.map((ing, i) => (
                <Grid key={i} container flexdirection="row" justifyContent="space-between" className={classes.root}>
                    <Grid item xs={8}>
                        <FormElement
                            name="title"
                            label="Title ingredient"
                            value={ing.title}
                            onChange={e => ingredientsChangeHandler(i, 'title', e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <FormElement
                            name="amount"
                            label="Amount ingredient"
                            value={ing.amount}
                            onChange={e => ingredientsChangeHandler(i, 'amount', e.target.value)}
                        />
                    </Grid>
                </Grid>
            ))}
            <Button
                className={classes.root}
                type="button"
                variant="contained"
                color="primary"
                onClick={addIngredients}
            >
               Add new ingredient
            </Button>
            <Grid item xs>
                <TextField
                    type="file"
                    name="image"
                    onChange={fileChangeHandler}
                    error={Boolean(getFieldError('image'))}
                    helperText={getFieldError('image')}
                />
            </Grid>

            <Grid item xs={12}>
                <ButtonWithProgress
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    loading={loading}
                    disabled={loading}
                >
                    Create
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default CocktailForm;