import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import Layout from "./components/UI/Layout/Layout";
import Login from "./containers/Login/Login";
import HomePage from "./containers/HomePage/HomePage";
import NewCocktail from "./containers/NewCocktail/NewCocktail";
import Cocktail from "./containers/Cocktail/Cocktail";

const App = () => {
  const user = useSelector(state => state.users.user);

  const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props}/> :
        <Redirect to={redirectTo}/>
  };

  return (
      <Layout>
        <Switch>
          <Route path="/login" component={Login}/>
          <ProtectedRoute
              path="/"
              component={HomePage}
              isAllowed={user}
              redirectTo="/login"
              exact
          />
            <ProtectedRoute
              path="/"
              component={HomePage}
              isAllowed={user}
              redirectTo="/login"
              exact
          />
          <ProtectedRoute
              path="/cocktail/:id"
              component={Cocktail}
              isAllowed={user}
              redirectTo="/login"
              exact
          />
            <ProtectedRoute
              path="/newCocktail"
              component={NewCocktail}
              isAllowed={user}
              redirectTo="/login"
              exact
          />
        </Switch>
      </Layout>
  );
};

export default App;
