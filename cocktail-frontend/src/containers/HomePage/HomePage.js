import React, {useEffect} from 'react';
import {Button, CircularProgress, Grid, makeStyles, Typography,} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useSelector, useDispatch} from "react-redux";
import CocktailItem from "../../components/CocktailItem/CocktailItem";
import {fetchCocktailsRequest} from "../../store/actions/cocktailsActions";

const useStyles = makeStyles(theme => ({
    title: {
        [theme.breakpoints.down('xs')]: {
            marginLeft: "50px",
        },
    }
}));


const HomePage = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const {user} = useSelector(state => state.users);
    const {fetchLoading, cocktails} = useSelector(state => state.cocktails);

    useEffect(() => {
        dispatch(fetchCocktailsRequest())
    }, [dispatch]);


    return (
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                <Grid item className={classes.title}>
                    <Typography variant="h4">Cocktails</Typography>
                </Grid>
                {user && (
                    <Grid item>
                        <Button coloe="primary" component={Link} to="/newCocktail">Add</Button>
                    </Grid>
                )}
            </Grid>
            <Grid item>
                <Grid item container justifyContent="center" direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : (
                        <Grid
                            container
                            flexdirection="column"
                        >
                            <Grid item xs={12} mt={4}>
                                <Typography variant="subtitle2">All Published</Typography>
                                {
                                    cocktails.map(c => (
                                        c.published && <CocktailItem
                                            key={c._id}
                                            id={c._id}
                                            title={c.title}
                                            image={c.image}
                                        />))
                                }
                            </Grid>
                            <Grid item xs={12} mt={4}>
                                <Typography variant="subtitle2">{user.role === 'admin' ? "Not published" : 'My not published'}</Typography>
                                {
                                    cocktails.map(c => (
                                        !c.published &&
                                        <CocktailItem
                                            key={c._id}
                                            id={c._id}
                                            title={c.title}
                                            image={c.image}
                                        />))
                                }
                            </Grid>
                            <Grid item xs={12} mt={4}>
                                <Typography variant="subtitle1">My Cocktails</Typography>
                                {
                                    cocktails.map(c => (
                                        c.creator === user._id &&
                                        <CocktailItem
                                            key={c._id}
                                            id={c._id}
                                            title={c.title}
                                            image={c.image}
                                        />))
                                }
                            </Grid>
                        </Grid>
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default HomePage;