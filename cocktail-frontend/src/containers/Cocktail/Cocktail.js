import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Button, Grid, Paper, Typography} from "@material-ui/core";
import {changeCocktailRequest, fetchCocktailRequest, removeCocktailRequest} from "../../store/actions/cocktailsActions";
import {apiURL} from "../../config";
import DeleteIcon from "@material-ui/icons/Delete";
import {FcSettings} from "react-icons/fc";

const Product = ({match}) => {
    const dispatch = useDispatch();
    const {cocktail} = useSelector(state => state.cocktails);
    const {user} = useSelector(state => state.users);

    useEffect(() => {
        dispatch(fetchCocktailRequest(match.params.id));
    }, [dispatch, match.params.id]);

    return cocktail && (
        <Paper component={Box} p={2}>
            {cocktail.image && <img src={apiURL + '/' + cocktail.image} alt="Cocktail"/>}
            <Typography variant="h4">{cocktail.title}</Typography>
            <Typography variant="subtitle2">{cocktail.recipe}</Typography>
            <Grid m={3}>
                {cocktail.ingredients.map(c => (
                    <Grid key={c._id}>
                        <Typography variant="body1">{c.title}</Typography>
                        <Typography variant="body1">{c.amount}</Typography>
                    </Grid>
                ))}
            </Grid>
            <Grid container justifyContent="space-between">
                <Button size="small" color="primary" onClick={() => dispatch(removeCocktailRequest(cocktail._id))}>
                    <DeleteIcon fontSize="small" /> Delete
                </Button>
                {!cocktail.published && <Button size="small" color="primary" onClick={() => dispatch(changeCocktailRequest(cocktail._id))}>
                    <FcSettings fontSize="small" />  Post
                </Button>}
            </Grid>
        </Paper>
    );
};

export default Product;