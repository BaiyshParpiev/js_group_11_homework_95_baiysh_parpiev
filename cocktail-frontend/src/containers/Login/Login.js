import React from 'react';
import {Avatar, Container, Grid, makeStyles, Typography} from "@material-ui/core";
import LockOpenOutlinedIcon from "@material-ui/icons/LockOpenOutlined";
import {useSelector} from "react-redux";
import {Alert} from "@material-ui/lab";
import GoogleLogin from "../../components/UI/GoogleLogin/GoogleLogin";
import FacebookLogin from "../../components/UI/FacebookLogin/FacebookLogin";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    alert: {
        marginTop: theme.spacing(3),
        width: '100%',
    },
}));
const Login = () => {
    const classes = useStyles();

    const {loginError} = useSelector(state => state.users);
    return (
        <Container component="section" maxWidth="xs">
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h6">
                    Sign In
                </Typography>
                {loginError &&
                    <Alert
                        className={classes.alert}
                        severity="error"
                    >{loginError.message || loginError.global}
                    </Alert>
                }
                <Grid
                      container
                      className={classes.form}
                      spacing={2}
                >
                    <Grid item xs={12}>
                        <FacebookLogin/>
                    </Grid>
                    <Grid item xs={12}>
                        <GoogleLogin/>
                    </Grid>
                </Grid>
            </div>
        </Container>
    );
};

export default Login;