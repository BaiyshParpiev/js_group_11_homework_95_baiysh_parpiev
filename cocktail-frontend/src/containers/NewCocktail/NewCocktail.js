import React from 'react';
import {Typography} from "@material-ui/core";
import CocktailForm from "../../components/CoctailForm/CocktailForm";
import {createCocktailRequest} from "../../store/actions/cocktailsActions";
import {useDispatch, useSelector} from "react-redux";

const NewCocktail = () => {
    const dispatch = useDispatch();
    const {singleLoading, singleError} = useSelector(state => state.cocktails);

    const onSubmit = productData => {
        dispatch(createCocktailRequest(productData))
    };

    return (
       <>
           <Typography variant="h4">New cocktail</Typography>
           <CocktailForm
               onSubmit={onSubmit}
               error={singleError}
               loading={singleLoading}
           />
       </>
    );
};

export default NewCocktail;