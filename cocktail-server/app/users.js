const express = require('express');
const User = require('../models/User');
const config = require('../config');
const {OAuth2Client} = require('google-auth-library')
const {nanoid} = require("nanoid");
const axios = require("axios");
const client = new OAuth2Client(config.google.clientId);

const router = express.Router();



router.post('/googleLogin', async (req, res) => {
  try {
    const ticket = await client.verifyIdToken({
      idToken: req.body.tokenId,
      audience: config.google.clientId,
    });
    const {name, email, sub: ticketUserId} = ticket.getPayload();

    if (req.body.googleId !== ticketUserId) {
      return res.status(401).send({global: 'User ID incorrect!'})
    }
    let user = await User.findOne({email});
    if (!user) {
      user = new User({
        email,
        password: nanoid(),
        displayName: name
      })
    }

    user.generateToken();
    await user.save({validateBeforeSave: false});
    res.send({message: 'Success', user})
  } catch (e) {
    res.status(500).send({global: 'Server error, please try again'});
  }
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;

  const accessToken = config.facebook.appId + "|" + config.facebook.appSecret;

  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
  try {
    const response = await axios.get(debugTokenUrl);
    if (response.data.data.error) {
      return res.status(401).send({global: 'Facebook token incorrect!'});
    }

    if (req.body.id !== response.data.data.user_id) {
      return res.status(401).send({global: 'Wrong user Id'});
    }

    let user = await User.findOne({email: req.body.email});
    if (!user) {
      user = await User.findOne({facebookId: req.body.id});
    }


    if (!user) {
      user = new User({
        email: req.body.email || nanoid(),
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name,
        avatar: req.body.picture.data.url
      });

    }

    user.generateToken();
    await user.save({validateBeforeSave: false});
    res.send({message: 'Success', user});
  } catch {
    res.status(401).send({global: 'Facebook token incorrect!'});
  };
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};

  if (!token) return res.send(success);

  const user = await User.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save({validateBeforeSave: false});

  return res.send(success);
});



module.exports = router;





















