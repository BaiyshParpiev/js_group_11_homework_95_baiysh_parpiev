const express = require('express');
const multer = require('multer');
const Cocktail = require('../models/Cocktail');
const auth = require('../middleware/auth');
const path = require("path");
const {nanoid} = require('nanoid');
const config = require('../config');
const User = require('../models/User');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});


const upload = multer({storage});

router.get('/', auth, async(req, res) => {
    try{
        const cocktails = await Cocktail.find();
        res.send(cocktails);
    }catch{
        res.status(500).send({message: 'Something went wrong'});
    }
});

router.get('/:id', auth, async(req, res) => {
    try{
        const cocktail = await Cocktail.findById(req.params.id).populate('creator', 'displayName');
        if(!cocktail){
            return res.status(404).send({message: 'Cocktail not found'});
        }
        res.send(cocktail);
    }catch{
        res.status(500).send({message: 'Something went wrong'});
    }
});

router.post('/', auth, upload.single('image'), async(req, res) => {
    console.log(req.body.ingredients)
    try{
        const cocktail = {
            title: req.body.title,
            ingredients: JSON.parse(req.body.ingredients),
            recipe: req.body.recipe,
            creator: req.user._id,
        };

        console.log(cocktail)


        if(req.file){
            cocktail.image = 'uploads/' +  req.file.filename;
        }

        const cocktailNew = new Cocktail(cocktail);
            await cocktailNew.save();
            res.send(cocktailNew)

    }catch(e){
        console.log(e)
        res.status(400).send({error: e});
    }
});

router.put('/:id/published', auth, async(req, res) => {
    try{
        const user = await User.findById(req.user._id);
        if(user.role !== 'admin'){
            return res.status(403).send('You are not admin');
        }

        const cocktail = await Cocktail.findById(req.params.id);

        cocktail.published = true;
        await Cocktail.findByIdAndUpdate(req.params.id, cocktail, {new: true});
        res.send({message: 'Updated successful'})
    }catch(e){
        res.status(400).send({error: e});
    }
});

router.delete('/:id', auth, async(req, res) => {
    try{
        const user = await User.findById(req.user._id);
        if(user.role !== 'admin'){
            return res.status(403).send('You are not admin');
        }

        const cocktail = await Cocktail.findById(req.params.id);
        if(!cocktail) return res.status(400).send('Cocktail not found');

        await Cocktail.findByIdAndDelete(req.params.id);
        res.send({message: 'Deleted successful'})
    }catch(e){
        res.status(400).send({error: e});
    }
});



module.exports = router;