const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const IngredientsSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    amount: {
        type: String,
        required: true,
    }
})
const CocktailSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    recipe: {
        type: String,
        required: true,
    },
    ingredients: [IngredientsSchema],
    creator: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    published: {
        type: Boolean,
        default: false,
    },
    image: String,
});

CocktailSchema.plugin(idValidator);
const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;