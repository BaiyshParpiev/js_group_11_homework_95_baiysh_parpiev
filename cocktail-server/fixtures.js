const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Cocktail = require("./models/Cocktail");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [firstUser, secondUser] = await User.create({
    email: 'bayish.parpiev@gmail.com',
    password: 'baiysh',
    displayName: 'Baiysh',
    token: nanoid(),
    role: 'admin'
  }, {
    email: 'user@gmail.com',
    password: 'user',
    displayName: 'user',
    token: nanoid(),
    role: 'user'
  });

  await Cocktail.create({
    title: 'Mojito',
    recipe: 'Mix this classic cocktail for a party using fresh mint, white rum, sugar, zesty lime and cooling soda water. Play with the quantities to suit your taste',
    creator: firstUser,
    image: 'fixtures/mojito.png',
    ingredients: [{title: 'fat', amount: '0.5 kcal'}]
  }, {
    title: 'Sex on the beach',
    recipe: 'Combine vodka with peach schnapps and cranberry juice to make a classic sex on the beach cocktail. Garnish with cocktail cherries and orange slices',
    creator: secondUser,
    image: 'fixtures/sex_on_the_beach.png',
    ingredients: [{title: 'vodka', amount: '100 ml'}]
  })

  await mongoose.connection.close();
};

run().catch(console.error);